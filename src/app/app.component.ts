import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Mostrar el contenido con one-way binding';
  title2 = 'BLUMBIT MAYUSCULAS';
  precio = 123.45;
  today = new Date();
  personMessage = 'Hola soy Andrés y ';
}
