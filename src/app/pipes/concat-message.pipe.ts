import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'concatMessage'
})
export class ConcatMessagePipe implements PipeTransform {

  transform(value: string): string {
    return value + ' realicé un pipe';
  }
}
